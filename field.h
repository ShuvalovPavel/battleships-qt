#ifndef FIELD_H
#define FIELD_H

#pragma once

#include <iostream>
#include <time.h>
#include <conio.h>
#include <Qvector>

using namespace std;

class AnyField
{
private:
    QVector<int> ships;
public:
    QVector<QVector<int> > arr;
    AnyField();
    ~AnyField();
    void refresh();
    //void CreateUserField();
    void CreatePcField();
    int Check_Shot(int i, int j);//1-mimo 2 ranil 3-ybil
    void Show();
};


#endif // FIELD_H
