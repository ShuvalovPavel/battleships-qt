#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    scene = new QGraphicsScene(this);
    ui->graphicsView->setScene(scene);

    for (int i = 0; i < 10; i++)
    {
        QVector<int> vec;
        for (int j = 0; j < 20; j++)
            vec.push_back(0);
        fin.push_back(vec);
    }

    i_User = j_User = i_Pc = j_Pc = 0;
    Pc_c = User_c = 0;
    BotWin = HumanWin = 0;
    button=0;
    PcField.CreatePcField();
    Show();
    ShipStatus=0;
    for (int i = 0; i <=4; i++)
        ships.push_back(0);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::mousePressEvent(QMouseEvent *event)
{
    QPoint viewPos = event->pos();
    QPointF scenePos=ui->graphicsView->mapToScene(viewPos);
    x1=scenePos.x()-1;
    y1=scenePos.y()-13;
    if(button==0){
        if ((x1>=0)&&(y1>=0)&&(x1<=200)&&(y1<=199)){
            x1=(x1)/20+1;
            y1=(y1)/20+1;
            UserField.arr[y1][x1]=2;
            Show();
        }
    }
    else{
        x1=x1-220;
        if ((x1>=0)&&(y1>=0)&&(x1<=200)&&(y1<=199)){
            x1=(x1)/20+1;
            y1=(y1)/20+1;
            i_User=y1;
            j_User=x1;

            if(Human.User_Cell_Pick(PcFieldForUser, i_User, j_User)==true){
                User_c = PcField.Check_Shot(i_User, j_User);
                Human.Player_Cell_Change(PcFieldForUser, i_User, j_User, User_c);
                if (User_c == 2)
                    HumanWin++;
                Show();
                if (User_c == 0){
                    do{
                        Bot.Pc_Cell_Pick(UserFieldForPc, &i_Pc, &j_Pc, i_Pc_Hit, j_Pc_Hit, ShipStatus);
                        Pc_c = UserField.Check_Shot(i_Pc, j_Pc);
                        if(Pc_c==1){
                            ShipStatus=1;
                            i_Pc_Hit=i_Pc;
                            j_Pc_Hit=j_Pc;
                        }
                        if(Pc_c==2)
                            ShipStatus=0;
                        Bot.Player_Cell_Change(UserFieldForPc, i_Pc, j_Pc, Pc_c);
                        if (Pc_c == 2)
                            BotWin++;
                    }while(Pc_c!=0);
                }
                Show();
                if (BotWin == 10){
                    QMessageBox::information(this, "LOSE", "You LOSE!");
                    Refresh();
                }
                if (HumanWin == 10){
                    QMessageBox::information(this, "WIN", "You Win!");
                    Refresh();

                }
            }
        }
    }
}


void MainWindow::on_pushButton_clicked()
{
    if (button==0){
        if(Check()){// ������ ����
            button++;
            ui->pushButton->setText("refresh");

        }
        else{
            QMessageBox::information(this, "Error", "Location impossible ships!");
            Refresh();
        }
    }
    else{
        Refresh();
    }
}

bool MainWindow::Check()
{
    for (int i = 1; i <=4; i++)
        ships[i]=5-i;
    int i1,i2,j1,j2;
    AnyField CheckFieldForUser;
    for (int ii = 1; ii < 11; ii++)
        for (int jj = 1; jj < 11; jj++)
            CheckFieldForUser.arr[ii][jj]=UserField.arr[ii][jj];//CheckFieldForUser=UserField;
    for (int ii = 1; ii < 11; ii++)
        for (int jj = 1; jj < 11; jj++){
            if(CheckFieldForUser.arr[ii][jj]==2){
                i1=i2=ii;
                j1=j2=jj;
                while ((CheckFieldForUser.arr[i1][jj] == 2) && (i1 > 0))
                    i1--;
                i1++;
                while ((CheckFieldForUser.arr[i2][jj] == 2) && (i2 < 11))
                    i2++;
                i2--;
                while ((CheckFieldForUser.arr[ii][j1] == 2) && (j1 > 0))
                    j1--;
                j1++;
                while ((CheckFieldForUser.arr[ii][j2] == 2) && (j2 < 11))
                    j2++;
                j2--;

                for (int q = i1; q <=i2; q++)
                    for (int w = j1; w <=j2; w++)
                        CheckFieldForUser.arr[q][w]=0;
                if ((i2-i1==0)||(j2-j1==0)){
                    for (int q = i1-1; q <=i2+1; q++)
                        for (int w = j1-1; w <=j2+1; w++)
                            if (CheckFieldForUser.arr[q][w]==2)
                                return(false);
                }
                else
                    return(false);

                if ((i2-i1+1>4)||(j2-j1+1>4))
                    return(false);

                if (i2-i1==0)
                    ships[j2-j1+1]--;
                else
                    ships[i2-i1+1]--;
            }//
        }
    for (int k=1;k<=4;k++)
        if (ships[k]!=0)
            return(false);
    return(true);
}

void MainWindow::Show(){

    int t1,t2;
    t1=t2=0;
    for (int i = 1; i < 11; i++)
        for (int j = 1; j < 11; j++)
            if (UserField.arr[i][j] == 2)
                fin[i-1][j-1] = 2;
    for (int i = 1; i < 11; i++)
        for (int j = 1; j < 11; j++){
            if (UserFieldForPc.arr[i][j] == 1)
                fin[i-1][j-1] = 1;
            if (UserFieldForPc.arr[i][j] == 2)
                fin[i-1][j-1] = 3;
        }
    for (int i = 1; i < 11; i++)
        for (int j = 1; j < 11; j++){
            if (PcFieldForUser.arr[i][j] == 1)
                fin[i - 1][j + 9] = 1;
            if (PcFieldForUser.arr[i][j] == 2)
                fin[i - 1][j + 9] = 3;
        }
    int x, y;
    x=0; y=0;
    QBrush redBrush(Qt::red);
    QBrush blueBrush(Qt::blue);
    QBrush blackBrush(Qt::black);
    QPen blackPen(Qt::black);
    blackPen.setWidth(1);
    for(int j=1; j<=10; j++){
        grText=scene->addText(QString::number(j),QFont("Times new roman",10,QFont::Bold));
        grText->setPos(-20,y);
        y=y+20;
    }
    y=0;
    for(int j=1; j<=10; j++){
        grText=scene->addText(QString::number(j),QFont("Times new roman",10,QFont::Bold));
        grText->setPos(200,y);
        y=y+20;
    }
    QString alphabet,string;
    string=" ";
    alphabet= " abcdefghij";
    for(int j=1; j<=10; j++){
        string[0]=alphabet[j];
        grText=scene->addText(string,QFont("Times new roman",10,QFont::Bold));
        grText->setPos(x,-20);
        x=x+20;
    }
    y=0;
    x=x+20;
    for(int j=1; j<=10; j++){
        string[0]=alphabet[j];
        grText=scene->addText(string,QFont("Times new roman",10,QFont::Bold));
        grText->setPos(x,-20);
        x=x+20;
    }

    for(int q=0; q<10; q++)
        for(int j=0; j<20; j++)
        {
            t1=t2=0;
            if(j>=10)
                t2=+20;

            if (fin[q][j]==0)
                rect = scene->addRect(j*20+t2,q*20+t1,18,18, blackPen, blueBrush);
            if (fin[q][j]==1){
                rect = scene->addRect(j*20+t2,q*20+t1,18,18, blackPen, blueBrush);
                ellipse = scene->addEllipse(j*20+t2+4,q*20+t1+4,7,7, blackPen, blackBrush);
            }
            if (fin[q][j]==2){
                rect = scene->addRect(j*20+t2,q*20+t1,18,18, blackPen, blueBrush);
                rect = scene->addRect(j*20+t2+1,q*20+t1+1,16,16, blackPen, blueBrush);
                rect = scene->addRect(j*20+t2+2,q*20+t1+2,14,14, blackPen, blueBrush);
                rect = scene->addRect(j*20+t2+3,q*20+t1+3,12,12, blackPen, blueBrush);
            }
            if (fin[q][j]==3)
                rect = scene->addRect(j*20+t2,q*20+t1,18,18, blackPen, redBrush);


        }
}

void MainWindow::mouseMoveEvent(QMouseEvent *event)
{
    QPoint viewPos = event->pos();
    QPointF scenePos=ui->graphicsView->mapToScene(viewPos);
    x1=scenePos.x();
    y1=scenePos.y();
    x1=x1; y1=y1;
    if ((x1>=0)&&(y1>=0)&&(x1<=199)&&(y1<=198))
        x1=(x1)/20; y1=(y1)/20;
}

void MainWindow::on_pushButton_2_clicked()
{
    close();
}

void MainWindow::Refresh(){
    ui->pushButton->setText("start");
    i_User = j_User = i_Pc = j_Pc = 0;
    Pc_c = User_c = 0;
    BotWin = HumanWin = 0;
    ShipStatus=0;
    UserField.refresh();
    PcField.refresh();
    PcFieldForUser.refresh();
    UserFieldForPc.refresh();
    PcField.CreatePcField();
    for (int i = 0; i < 10; i++)
        for (int j = 0; j < 20; j++)
            fin[i][j] = 0;
    Show();
    button=0;
}
