#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <iostream>
#include <locale.h>
#include <time.h>
#include <conio.h>
#include <vector>
//..................................................
#include <QMainWindow>
#include <QtCore>
#include <QtGui>
#include <QWidget>
#include <QPainter>
#include <QGraphicsView>
#include <QList>
#include <QPointF>
#include <QStringList>
 #include <QVector>
#include <QMouseEvent>
//#include <cstdlib>
#include <iostream>
#include "qmessagebox"
#include "field.h"
#include "player.h"


using namespace std;
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    QVector<int> ships;//0,4,3,2,1
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void Show();
    bool Check();
    void Refresh();
private slots:
    void on_pushButton_clicked();
    void on_pushButton_2_clicked();

protected:
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
private:
    Ui::MainWindow *ui;
    QGraphicsScene *scene;
    QGraphicsRectItem * rect;
    QGraphicsEllipseItem * ellipse;
    QGraphicsTextItem * grText;
    int i_User, j_User, i_Pc, j_Pc;
    int Pc_c, User_c;//0-����, 1-�����, 2-����
    int BotWin, HumanWin;//������� ������
    int x1, y1;//������� �������
    int button;
    int ShipStatus, i_Pc_Hit, j_Pc_Hit;//������������ ���������� �������� ��������, ���� Pc_c!=2
    QVector<QVector<int> > fin;
    AnyField UserField;
    AnyField PcField;
    AnyField PcFieldForUser;
    AnyField UserFieldForPc;
   // AnyField ShowUserField;
    Player Bot;
    Player Human;
};



#endif // MAINWINDOW_H



