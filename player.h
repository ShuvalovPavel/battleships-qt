#ifndef PLAYER_H
#define PLAYER_H

#pragma once

#include <iostream>
#include <time.h>
#include <conio.h>
#include <QVector>
#include "field.h"

using namespace std;

class Player
{
private:
    int i1, i2, j2, j1;
public:
    Player();
    ~Player();
    bool User_Cell_Pick(AnyField PcFieldForUser, int i_User, int j_User);
    void Pc_Cell_Pick(AnyField UserFieldForPc, int *i_Pc, int *j_Pc, int i_Pc_Hit, int j_Pc_Hit, int c);
    void Player_Cell_Change(AnyField & FieldForPlayer, int i, int j, int c);
};



#endif // PLAYER_H
