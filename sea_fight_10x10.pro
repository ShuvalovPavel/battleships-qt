#-------------------------------------------------
#
# Project created by QtCreator 2015-08-31T10:38:13
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = sea_fight_10x10
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    field.cpp \
    player.cpp

HEADERS  += mainwindow.h \
    field.h \
    player.h

FORMS    += mainwindow.ui
